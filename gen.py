#!/usr/bin/env python3

import pyotp
import time
import datetime
from os import system, name, environ
import json
from prettytable import PrettyTable

# Function to clear the screen
def clear():
  if name == 'nt':
    _ = system('cls')
  else:
    _ = system('clear')


if __name__ == "__main__":
  otpJSON = json.loads(environ["GOOGLE_OTP_GEN"])
  otpJSONkeys = otpJSON.keys()

  while True:
    clear()
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    content = PrettyTable()
    content.field_names = ["Description","OTP"]

    for key in otpJSONkeys:
      totp = pyotp.TOTP(otpJSON[key])
      content.add_row( [ key, totp.now() ] )
    print(content)

    time.sleep(1)
